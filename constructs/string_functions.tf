variable "input_string" {
    default = "Terraform"
}

variable "greet_user" {
    default = "Hello, Naveen"
}

variable "last_name" {
  default = "Kumar"
}

output "input_string"{
    value = var.input_string
}

output "input_string_length"{
    value = length(var.input_string)
}

output "input_string_upper"{
    value = upper(var.input_string)
}

output "input_string_title"{
    value = title(var.input_string)
}

output "input_string_substring"{
    value = substr(var.input_string, 2,4)
}

output "input_string_replace"{
    value = replace(var.greet_user, "Naveen", "Suraj")
}

output "input_string_concat"{
    value = concat([var.greet_user],[", "], [var.last_name])
}