locals {
  greeting = "Hello"
  env = "dev"
  version = "1.0.0"
}

resource "random_integer" "priority" {
  min = 1
  max = 50000
}


output "hello_world" {
  value = "${local.greeting}, World!"
}

output "env_log" {
  value = "env - ${local.env}"
}

output "version_print" {
  value = "Version - ${local.env}"
}

output "current_timestamp" {
  value = timestamp()
}

output "formatted_date" {
  value = formatdate("YYYY-MM-DD", "2022-01-01T12:00:00Z")
}

output "random_generated_number" {
  sensitive = true
  value = random_integer.priority.result
}


resource "random_password" "password" {
  length           = 16
  special          = true
  override_special = "!#$%&*()-_=+[]{}<>:?"
}



output "random_generated_password" {
  sensitive = true
  value = random_password.password.result
}




