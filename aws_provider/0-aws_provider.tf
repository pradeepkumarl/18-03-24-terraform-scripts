terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "ap-south-1"
}

resource "aws_instance" "pradeep_instance" {
  ami           = "ami-05295b6e6c790593e"  # Change this to the desired AMI ID
  instance_type = "t2.small"              # Change this to the desired instance type
  key_name = "pradeep-ec2-connect"
  vpc_security_group_ids = [aws_security_group.instance_sg.id]

  tags = {
    Name = "pradeep-EC2-instance"
  }
}

resource "aws_security_group" "instance_sg" {
  name        = "InstanceSecurityGroup"
  description = "Security group for EC2 instance"
  
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] # Be cautious with this setting; restrict to trusted IPs in production
  }
}


output "instance_ips" {
  value = aws_instance.pradeep_instance.public_ip
}
output "instance_public_dns" {
  value = aws_instance.pradeep_instance.public_dns
}
