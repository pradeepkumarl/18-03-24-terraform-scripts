
variable "user_names" {
    type = list(string)
    default = ["user1","user2", "user3"]
}

resource "aws_iam_group" "admin_group" {
    name = "admin-group"
}

resource "aws_iam_group_policy_attachment" "admin_group_attachment" {
    group = aws_iam_group.admin_group.name
    policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
}

resource "aws_iam_user" "all_users" {
    for_each = { for user_name in var.user_names: user_name => user_name}
    name = each.value
}

resource "aws_iam_user_group_membership" "example_users_group_membership" {
    for_each = aws_iam_user.all_users

    user = each.value.name
    groups = [aws_iam_group.admin_group.name]
}