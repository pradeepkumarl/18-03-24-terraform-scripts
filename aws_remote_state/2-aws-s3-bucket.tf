terraform {
    backend "s3" {
        bucket = "terraform-training-state-repo"
        key = "terraform.tfstate"
        region = "ap-south-1"
        dynamodb_table = "terraform_locking_table"
    }
}
