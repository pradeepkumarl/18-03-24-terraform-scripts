terraform {
    backend "s3" {
        bucket = "terraform-training-state-repo"
        key = "vpc.terraform.tfstate"
        region = "ap-south-1"
        dynamodb_table = "terraform_locking_table"
    }
}
provider "aws" {
  region  = "ap-south-1"
}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}
