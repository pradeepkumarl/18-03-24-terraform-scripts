resource "aws_vpc" "eks_vpc" {
  cidr_block = "10.0.0.0/24"

  tags = {
    Name = "eks_vpc"
  }
}
resource "aws_iam_user" "test_user" {
  name = "john_doe"
}
